let disciplinas = [];
class Disciplina {
  constructor(nome, conteudo, cargaHoraria, professor) {
    this.nome = nome;
    this.conteudo = conteudo;
    this.cargaHoraria = cargaHoraria;
    this.professor = professor;
  }
}

function cadastrarDisciplina() {
  let nome = byId("nomeDaDisciplina").value;
  let conteudo = byId("conteudo").value;
  let cargaHoraria = byId("cargaHoraria").value;
  let professor = byId("professor").value;
  let disciplina = new Disciplina(nome, conteudo, cargaHoraria, professor);
  disciplinas.push(disciplina);
  mostrarDisciplinas();
}

function mostrarDisciplinas() {
  let tabelaDeDisciplinas = byId("tabelaDeDisciplinas");
  let lista  = "<ol>"
  for(let disciplina of disciplinas) {
    lista += `<li>${disciplina.nome} (${disciplina.cargaHoraria}): ${disciplina.conteudo} (${disciplina.professor}); </li>`
  }
  lista += "</ol>" 
  tabelaDeDisciplinas.innerHTML = lista;
}

function byId(id) {
  return document.getElementById(id);
}